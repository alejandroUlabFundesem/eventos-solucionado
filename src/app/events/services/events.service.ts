import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IEvent } from '../interfaces/i-event';
import { HttpClient } from '@angular/common/http';
import { EventResponse, OkResponse } from '../interfaces/responses';
import { SERVICES } from '../events.constants';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  private eventURL = SERVICES + '/events';

  constructor(private http: HttpClient) { }

  getEvents(): Observable<IEvent[]> {
    return this.http.get<{events: IEvent[]}>(this.eventURL).pipe(
      map(response => response.events)
    );
  }

  getEvent(idEvent: number): Observable<IEvent> {
    return this.http.get<EventResponse>(this.eventURL + '/' + idEvent).pipe(
      map(response => {
        if (response.ok)
          return response.event;
        else
          throw response.errors;
      })
    );
  }

  addEvent(event: IEvent): Observable<IEvent> 
  {
    return this.http.post<EventResponse>(this.eventURL, event).pipe(
      map(response => {
        if (response.ok)
          return response.event;
        else
          throw response.errors;
      })
    );
  }

  deleteEvent(idEvent: number): Observable<boolean> 
  {
    return this.http.delete<OkResponse>(this.eventURL + '/' + idEvent).pipe(
      map(response => {
        if (response.ok)
          return true;
        else
          throw response.error;
      })
    );
  }

}
