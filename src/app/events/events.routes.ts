import { Route } from '@angular/router';
import { EventAddComponent } from './event-add/event-add.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { EventsShowComponent } from './events-show/events-show.component';
import { EventDetailResolveGuard } from './guards/event-detail-resolve.guard';
import { SaveChangesGuard } from './guards/save-changes.guard';

export const EVENTS_ROUTES: Route[] = [
    { path: '', component: EventsShowComponent },
    { path: 'add', component: EventAddComponent, canDeactivate: [SaveChangesGuard] },
    { path: ':id', component: EventDetailComponent, resolve: { event: EventDetailResolveGuard } },
];