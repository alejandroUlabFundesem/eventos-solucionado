import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IEvent } from '../interfaces/i-event';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss']
})
export class EventDetailComponent implements OnInit {

  event: IEvent = null;

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.event = this.route.snapshot.data['event'];
  }

  goBack() {
    this.router.navigate(['/events']);
  }
}
