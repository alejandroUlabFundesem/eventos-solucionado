import { Component, OnInit } from '@angular/core';
import { IEvent } from '../interfaces/i-event';
import { EventsService } from '../services/events.service';

@Component({
  selector: 'app-events-show',
  templateUrl: './events-show.component.html',
  styleUrls: ['./events-show.component.scss']
})
export class EventsShowComponent implements OnInit {

  events: IEvent[] = [];

  filterSearch: string = ''; 

  constructor(private eventsService: EventsService) { }

  ngOnInit() {
    this.eventsService.getEvents().subscribe(
      events => this.events = events, 
      error => console.error(error),
      () => console.log("Events loaded")
    );
  }

  orderDate(event: Event) {
    event.preventDefault();
    this.filterSearch = '';

    this.events.sort((event1, event2) => event1.date > event2.date ? -1 : 1 )
  }

  orderPrice(event: Event) {
    event.preventDefault();
    this.filterSearch = '';

    this.events.sort((event1, event2) => event1.price > event2.price ? -1 : 1 )
  }

  deleteEvent(deletedEvent) {
    this.events = this.events.filter(event => event.id !== deletedEvent.id);
  }
}
