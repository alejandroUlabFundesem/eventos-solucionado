import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmModalComponent } from 'src/app/shared/confirm-modal/confirm-modal.component';
import { IEvent } from '../interfaces/i-event';
import { EventsService } from '../services/events.service';

@Component({
  selector: 'app-event-item',
  templateUrl: './event-item.component.html',
  styleUrls: ['./event-item.component.scss']
})
export class EventItemComponent implements OnInit {

  @Input() event: IEvent;

  @Output() deletedEvent = new EventEmitter<IEvent>();

  constructor(
    private eventsService: EventsService, 
    private modalService: NgbModal) { }

  ngOnInit() {
  }

  deleteEvent() {
    const modalRef = this.modalService.open(ConfirmModalComponent);
    modalRef.componentInstance.title = 'Eliminar evento';
    modalRef.componentInstance.body = '¿Estás seguro de que deseas eliminar el evento?';
    modalRef.result
      .then(respuesta => {
        if (respuesta === true) {
          this.eventsService.deleteEvent(this.event.id).subscribe(
            resultado => {
              if (resultado === true)
                this.deletedEvent.emit(this.event);
            },
            error => console.error(error),
            () => console.log("Event deleted")
          );
        }
      });
  }
}
