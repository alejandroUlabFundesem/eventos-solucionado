import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmModalComponent } from 'src/app/shared/confirm-modal/confirm-modal.component';
import { ComponentDeactivate } from '../interfaces/component-deactivate';
import { IEvent } from '../interfaces/i-event';
import { EventsService } from '../services/events.service';

@Component({
  selector: 'app-event-add',
  templateUrl: './event-add.component.html',
  styleUrls: ['./event-add.component.scss']
})
export class EventAddComponent implements OnInit, ComponentDeactivate {

  @ViewChild('eventForm', { static: true }) eventForm: NgForm;
  fechaHoy: string = new Date().toLocaleDateString();
  created: boolean = false;

  newEvent: IEvent = {
    title: '',
    description: '',
    image: '',
    price: 0,
    date: ''
  };

  constructor(
    private eventsService: EventsService, 
    private router: Router, 
    private modalService: NgbModal
  ) { }

  ngOnInit() {
  }

  createEvent() {
    if(this.eventForm.valid) {
      this.eventsService.addEvent(this.newEvent).subscribe(
        event => {
          this.newEvent = {
            title: '',
            description: '',
            image: '',
            price: 0,
            date: ''
          };
  
          this.created = true;
          this.router.navigate(['/events']);
        },
        error => console.error(error),
        () => console.log("Event created")
      );
    }
  }

  changeImage(fileInput: HTMLInputElement) {
    if (!fileInput.files || fileInput.files.length === 0) { return; }
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(fileInput.files[0]);
    reader.addEventListener('loadend', e => {
        this.newEvent.image = reader.result.toString();
    });
  }

  canDeactivate() : Promise<boolean> {

    if (this.created) 
      return new Promise((resolve, reject) => resolve(true));  
    
    const modalRef = this.modalService.open(ConfirmModalComponent);
    modalRef.componentInstance.title = 'Añadir evento';
    modalRef.componentInstance.body = 'Los cambios no se guardarán. ¿Desea salir?';
    return modalRef.result // Cuando cerramos con close → booleano recibido
            .catch(() => false); // dismiss -> Promise<false>
  }    

  validClasses(ngModel: NgModel) {
    return {
      ['is-valid']: ngModel.touched && ngModel.valid,
      ['is-invalid']: ngModel.touched && ngModel.invalid
    };
  }
}
