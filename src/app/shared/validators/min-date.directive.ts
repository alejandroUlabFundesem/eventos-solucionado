import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';

@Directive({
  selector: '[minDate]',
  providers: [ {provide: NG_VALIDATORS, useExisting: MinDateDirective, multi: true} ]
})
export class MinDateDirective implements Validator {

  @Input() minDate;
  
  constructor() { }

  validate(control: AbstractControl): ValidationErrors {
            if (this.minDate && control.value) { // Si recibimos algún valor
              const dateControl = new Date(control.value); // Valor actual
              const dateMin = new Date(this.minDate); // Fecha mínima
              if (dateMin > dateControl) {
                return {'minDate': true}; // Error devuelto
              }
            }
            
            return null; // Sin error
  }
}
